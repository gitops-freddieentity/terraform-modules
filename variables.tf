variable "cidr" {
  description = "CIDR of the VPC"
  type        = string
  default     = "10.0.0.0/16"
}
variable "name" {
  description = "The common name of the VPC"
  type        = string
  default     = "my-vpc"
}
variable "public_subnets" {
  description = "An List of Objects containing configuration about subnet"
  type        = list(any)
  default = [
    {
      "name" : "public-subnet-1",
      "cidr" : "10.0.1.0/24",
      "az" : "us-east-1a",
      "tags" : {}
    }
  ]
}
variable "private_subnets" {
  description = "An List of Objects containing configuration about subnet"
  type        = list(any)
  default = [
    {
      "name" : "private-subnet-1",
      "cidr" : "10.0.101.0/24",
      "az" : "us-east-1a",
      "tags" : {}
    }
  ]
}
variable "public_subnet_tags" {
  description = "Common tag for public subnets"
  type        = map(any)
  default     = {}
}
variable "private_subnet_tags" {
  description = "Common tag for private subnets"
  type        = map(any)
  default     = {}
}
variable "single_nat_gateway" {
  description = "Enable a single NAT Gateway for private subnets"
  type        = bool
  default     = true
}